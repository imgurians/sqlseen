#!/bin/sh
# This is how we run the importer from cron to make sure there is no
# half-finished inconsistent database

if [ $# -ne 2 ]; then
    echo "wrong # parameters: $0 path/to/gseen.dat path/to/sqlite.db"
    exit 1
fi

gsfile="$1"
dbfile="$2"
dbtmpfile="$dbfile.tmp" # in the same directory for atomic rename(2)

rmfile() {
    rm -f "$dbtmpfile"
}

rmfile
trap rmfile INT TERM QUIT EXIT

gseen-import -n "$gsfile" "$dbtmpfile" &&
    test -f "$dbtmpfile" &&
    ln -f "$dbtmpfile" "$dbfile"

