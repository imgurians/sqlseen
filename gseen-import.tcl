#!/usr/bin/env tclsh

# Copyright (c) 2016   Moritz "ente" Wilhelmy

# See the LICENSE file included with the official distribution for the terms of
# the MIT/X Consortium License

# This script imports the gseen.mod flat-file database into an sqlite3 database.
# For the schema (which currently is closely related to the gseen.mod flat-file
# database but will evolve into something using more advanced sqlite features
# soon™), see below.

# Currently, incrementally updating the sql database with new data from the
# gseen.mod file is not yet supported

# Dependencies: Tcl 8.5 or later, sqlite3, tcllib

package require sqlite3
package require Tcl 8.5
package require try
package require cmdline 1.5

### Preparing the sqlite3 db

proc db_subst {{mapping ""} command} {
    # db eval with prior text substitution - handy for handling multiple
    # databases ("schemas" in sqlite nomenclature) attached to the same sqlite3
    # db handle because sqlite doesn't do variable expansion in CREATE TABLE
    # statements, apparently.
    db eval [string map $mapping $command]
}

proc prepare_db {name} {
    set mapping [list %DB% $name]
    db_subst $mapping {
        CREATE TABLE %DB%.seen(
          "id"      INTEGER PRIMARY KEY
        , "nick"    TEXT NOT NULL
        , "uhost"   TEXT NOT NULL
        , "chan"    TEXT NOT NULL
        , "type"    INTEGER NOT NULL
        , "when"    INTEGER NOT NULL
        , "spent"   INTEGER NOT NULL
        , "message" TEXT
        );
    }
    db_subst $mapping {
        CREATE TABLE %DB%.seen_requests(
          "id"      INTEGER PRIMARY KEY
        , "nick"    TEXT NOT NULL
        , "by"      TEXT NOT NULL
        , "host"    TEXT NOT NULL
        , "chan"    INTEGER NOT NULL
        , "when"    INTEGER NOT NULL
        );
    }

    db_subst $mapping {
        CREATE UNIQUE INDEX %DB%.unique_entries ON seen(id, nick);
        CREATE UNIQUE INDEX %DB%.unique_request_entries ON seen_requests(id, nick, by);
    }
}

### Helpers

proc status {chr} {
    if {$::nostatus} return
    puts -nonewline stderr $chr
    flush stderr
}

proc die {err fname {lineno ""}} {
    status "\n"
    if {$lineno != ""} {append lineno :}
    puts stderr "$fname:$lineno Error: $err"
    exit 1
}

### Main function

proc sqlimport {filename fd} {

    # Attach a second in-memory database for performance.
    db eval {ATTACH ":memory:" AS mem}
    prepare_db mem
    prepare_db main

    for {set lineno 1} {[gets $fd line] >= 0} {incr lineno} {
        if {[regexp {^#} $line]} {
            status #
            continue
        } elseif {[regexp {^! (\S+) (\S+) (\S+) (\d+) (\d+) ([-0-9]+)\s?(.*)$} $line -> nick uhost chan type when spent message]} {
            status !
            if {$message == ""} {unset message}
            db eval {INSERT INTO mem.seen("nick", "uhost", "chan", "type", "when", "spent", "message")
                VALUES(:nick, :uhost, :chan, :type, :when, :spent, :message)}
        } elseif {[regexp {^@ (\S+) (\S+) (\S+) (\S+) (\d+)$} $line -> nick by host chan when]} {
            status @
            db eval {INSERT INTO mem.seen_requests("nick", "by", "host", "chan", "when")
                VALUES(:nick, :by, :host, :chan, :when)}
        } else {
            die "unknown line type" $filename $lineno 
        }
    }

    # dump the in-memory database back into the on-disk database
    db eval {
        INSERT INTO main.seen SELECT * FROM mem.seen;
        INSERT INTO main.seen_requests SELECT * FROM mem.seen_requests;
    }
}

### Main toplevel code

# Argument handling
set options {
    {n          "no status output"}
}
set usage ": gseen-import.tcl \[options\] path/to/gseen.dat path/to/sqlite.db\noptions:"
try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    puts stderr $msg
    exit 1
}

if {[llength $argv] != 2} {
    puts stderr "Expected exactly two arguments."
    puts stderr [cmdline::usage $options $usage]
    exit 1
}
set filename [lindex $argv 0]
set dbfile [lindex $argv 1]

set ::nostatus $params(n)
if {$filename != "-"} {
    if {[catch {set infd [open $filename]}]} {
        die "Unable to open file for reading" $filename
    }
} else {
    set infd stdin
    set filename <stdin>
}

# Initialize db
sqlite3 db $dbfile

db eval {
    PRAGMA journal_mode=wal; -- faster than journal-mode (which is the default) but about as fast as memory
    PRAGMA foreign_keys=on;  -- we don't use it but it can't hurt
}

# Import
sqlimport $filename $infd

# Cleanup
status "\n"
db close
close $infd
exit 0
